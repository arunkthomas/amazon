package pages;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import com.aventstack.extentreports.ExtentTest;
import amazon.Base;
import amazon.Library;

public class KindlePage {
	
	private By productTitle = By.xpath("//h1[@id='title']/span");
	private By buyNow = By.cssSelector("input#buy-now-button");

	Base b = new Base();
	Library lib=new Library();
	
	// verifying kindlepage
	public void verifykindlepage(WebDriver driver, ExtentTest logger) throws IOException {
		try {
			if ((lib.getWebElement(productTitle, driver).getText().contains(lib.getFromExcel("productTitle")))) {
				logger.pass("Kindle product page is diplayed");
			}
			else {
				logger.fail("Failed to display Kindle page");
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// Click on Buy Now
	public void buyNow(WebDriver driver, ExtentTest logger) {
		try {
			if(lib.WebElementExist(driver, buyNow)){
				lib.getWebElement(buyNow, driver).click();
				logger.pass("clicked on BuyNow");
			}
			else {
				logger.fail("unable to find BuyNow");
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
