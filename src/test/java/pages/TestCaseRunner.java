package pages;

import java.io.IOException;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.MediaEntityModelProvider;

import amazon.Base;
import amazon.Library;

public class TestCaseRunner extends Base {
	
	private WebDriver driver =null;
	
	LandingPage landpg=new LandingPage();
	KindlePage kindpg = new KindlePage();
	Library lib = new Library();
	AmazonSignInPage amzsign = new AmazonSignInPage();

	@Parameters({"Browser"})
	@Test
	public void validateKindleBuyNow(String browser) throws IOException {
		try {
			this.driver = launchBrowser(driver,logger,browser);
			logger = report.createTest("Validating Kindle BuyNow Functionality");
			landpg.getLandingPage(driver,logger);
			landpg.navigateToKindle(driver,logger);
			kindpg.verifykindlepage(driver,logger);
			kindpg.buyNow(driver,logger);
			amzsign.verifySignInPage(driver,logger);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@AfterTest
	public void teardown() throws Exception {
		try {
			driver.quit();
			report.flush();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@AfterMethod
	public void tearDownMethod(ITestResult result) throws Exception {
		try {
			if(result.getStatus() == ITestResult.FAILURE) {
				logger.fail("Test failed", MediaEntityBuilder.createScreenCaptureFromPath(lib.screenShotCapture(driver)).build());
			}
			else if(result.getStatus()==ITestResult.SUCCESS) {
				logger.pass("Test passed", MediaEntityBuilder.createScreenCaptureFromPath(lib.screenShotCapture(driver)).build());
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
