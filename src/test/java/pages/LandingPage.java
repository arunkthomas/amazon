package pages;

import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import com.aventstack.extentreports.ExtentTest;
import amazon.Base;
import amazon.Library;

public class LandingPage {
	
	private By hamburgerMenu = By.xpath("//i[@class='hm-icon nav-sprite']");
	private By kindleMainMenu = By.xpath("//a/div [text()='Kindle']");
	private By kindleEReader= By.xpath("//li/a [text()='Kindle']");
	
	Base base=new Base();
	Library lib=new Library();
	
	// Verifying Landing page
	public void getLandingPage(WebDriver driver, ExtentTest logger) throws IOException {
		
		try {
			driver.get(base.getfrompropertyfile("url"));
			String title = driver.getTitle();
			logger.pass("Landing Page");
			boolean flag = (title.contains(lib.getFromExcel("landingPageTitle")));
			if (flag == true) {
				logger.pass("Amazon Landing page is loaded");
			}
			else {
				logger.fail("Amazon Landing page failed to load");
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	// Navigate to Kindlepage
	public void navigateToKindle(WebDriver driver, ExtentTest logger) {
		try {
			if(lib.WebElementExist(driver, hamburgerMenu)) {
				lib.getWebElement(hamburgerMenu,driver).click();
				logger.pass("Clicked on hamberger menu");
			}
			else {
				logger.fail("Failed to click on hamberger menu");
			}
			if(lib.WebElementExist(driver, kindleMainMenu)) {
				lib.getWebElement(kindleMainMenu,driver).click();
				logger.pass("Clicked on Kindle main menu");
			}
			else {
				logger.fail("Failed to click on Kindle main menu");
			}
			if(lib.WebElementExist(driver, kindleEReader)) {
				lib.getWebElement(kindleEReader,driver).click();
				logger.pass("Clicked on Kindle EReader menu");
			}
			else {
				logger.fail("Failed to click on Kindle EReader menu");
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

}
