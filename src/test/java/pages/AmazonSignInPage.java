package pages;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.aventstack.extentreports.ExtentTest;

import amazon.Library;

public class AmazonSignInPage {
	
	private By signIn= By.xpath("//h1[@class='a-spacing-small']");
	private By emailfieldtitle= By.xpath("//label[@class='a-form-label']");
	
	Library lib=new Library();

	public void verifySignInPage(WebDriver driver, ExtentTest logger) throws IOException {
		try {
			String actualTitle = driver.getTitle();
			String expectedTitle= lib.getFromExcel("signInPageTitle");
			if (actualTitle.equals(expectedTitle)) {
				logger.pass("Sign in page displayed");
			}
			else {
				logger.fail("failed to display sign in page");
			}
			String actualSigninTitle_lbl = lib.getWebElement(signIn, driver).getText();
			String expectedSigninTitle_lbl = lib.getFromExcel("signInTitle");
			if(actualSigninTitle_lbl.equals(expectedSigninTitle_lbl)) {
				logger.pass("Sign-In Label displayed");
			}
			else {
				logger.fail("failed to display sign-in Label");
			}
			
			String actualEmailTitle_lbl = lib.getWebElement(emailfieldtitle, driver).getText();
			String expectedEmailTitle_lbl = lib.getFromExcel("fieldTitle");
			if(actualEmailTitle_lbl.equals(expectedEmailTitle_lbl)) {
				logger.pass("Email/phone number label displayed");
			}
			else {
				logger.fail("failed to display Email/phone number label");
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
	}	
	

}
