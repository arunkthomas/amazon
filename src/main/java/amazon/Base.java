package amazon;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class Base {
	Properties propertyfile;
	public ExtentTest logger;
	public ExtentReports report;
	ThreadLocal<ExtentTest> extentTest = new ThreadLocal<>();
	
	// method for getting data from excel sheet
	public String getfrompropertyfile(String key) throws IOException {
		try {
			FileInputStream propertyfilepath = new FileInputStream ("./src\\main\\resources\\properties.file");
			propertyfile = new Properties();
			propertyfile.load(propertyfilepath);
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return propertyfile.getProperty(key).trim();
		
	}
	
	//Initializing driver by selecting required browser
	public WebDriver launchBrowser(WebDriver driver, ExtentTest logger, String browser) throws IOException {
		logger = report.createTest("Browser Initiation");
		
		try {
			if (browser.equalsIgnoreCase("chrome")) {
				
				System.setProperty("webdriver.chrome.driver", "./chromedriver.exe");
				driver = new ChromeDriver();
				logger.pass("Successfully launched chrome");
			}
			else if (browser.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.gecko.driver", "./geckodriver.exe");
				driver = new FirefoxDriver();
				logger.pass("Successfully launched firefox");
				}
			else if (browser.equalsIgnoreCase("IE")) {
				System.setProperty("webdriver.chrome.driver", "./IEDriverServer.exe");
				driver = new InternetExplorerDriver();
				logger.pass("Successfully launched IE");
				}
			else {
				logger.fail("Unable to launch browser");
			}
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		driver.manage().window().maximize();
		return driver;
	}
	@BeforeTest
	public void setupReporter() {
		String reportpath= System.getProperty("user.dir")+"\\reports\\index.html";
		ExtentHtmlReporter reporter = new ExtentHtmlReporter(reportpath);
		extentTest.set(logger);
		report=new ExtentReports();
		report.attachReporter(reporter);
	}
}

