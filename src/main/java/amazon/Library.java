package amazon;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.FileHandler;

import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.NumberToTextConverter;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.ExtentTest;
import com.thoughtworks.qdox.tools.QDoxTester.Reporter;

public class Library extends Base{
	
	//Method to get explicit wait
	public void waitUntilElement(WebDriver driver, By locator) {
		WebDriverWait wait= new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfAllElements(driver.findElement(locator)));
	}
	
	// Wrapper method to get the value corresponding to the keyword from Excel
	public String getFromExcel(String keyword) throws IOException {
		ArrayList<String> celldata= new ArrayList<String>();
		try {
			FileInputStream excelpath =new FileInputStream ("./src\\main\\resources\\TestData.xlsx");
			XSSFWorkbook workbook = new XSSFWorkbook(excelpath);
			int sheets= workbook.getNumberOfSheets();
			for (int i=0;i<sheets;i++) {
				if (workbook.getSheetName(i).equalsIgnoreCase(getfrompropertyfile("excelSheetName"))) {
					XSSFSheet sheet = workbook.getSheetAt(i);
					Iterator<Row> rows = sheet.iterator();
					Row firstrow = rows.next();
					Iterator<Cell> cells = firstrow.cellIterator();
					int k=0;
					int column =0;
					while(cells.hasNext()) {
						Cell value = cells.next();
						if (value.getStringCellValue().equalsIgnoreCase(getfrompropertyfile("titleColumn"))) {
							column = k;
						}
						k++;
			
					}
					while(rows.hasNext()) {
						Row rowValue = rows.next();
						if (rowValue.getCell(column).getStringCellValue().equalsIgnoreCase(keyword)) {
							Iterator <Cell> cellValues=rowValue.cellIterator();
							while (cellValues.hasNext()) {
								Cell c=cellValues.next();
								if (c.getCellType()==CellType.STRING) {
									celldata.add(c.getStringCellValue());
								}
								else {
									celldata.add(NumberToTextConverter.toText(c.getNumericCellValue()));
								}
							}
						}
					}
				}
			}
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return celldata.get(1);
	}
	
	// Wrapper method to get the webelement 
	public WebElement getWebElement(By element, WebDriver driver) {
		return driver.findElement(element);
	}
	
	
	//Wrapper method for checking if WebElement exsist or not
	public boolean WebElementExist (WebDriver driver, By element) {
		waitUntilElement(driver, element);
		boolean flag=driver.findElements(element).isEmpty();
		if (flag==false) {
			flag=true;
		}
		else {
			flag=false;
		}
		return flag;
	}
	
	// method to capture screen shot
	public String screenShotCapture(WebDriver driver) throws Exception{
		String screenShotPath = null;
		try {
			File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			screenShotPath = System.getProperty("user.dir")+"./screenshots/"+".png";
			try {
				FileUtils.copyFile(scrFile, new File(screenShotPath));
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return screenShotPath;
	}    

}
